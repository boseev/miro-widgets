package com.widget;

import com.widget.data.Widget;

public interface WithTestData {

    int DEF_X = 10;
    int DEF_Y = 20;
    int DEF_Z = 10;

    default Widget createWidget(Integer zIndex) {
        return new Widget().setX(DEF_X).setY(DEF_Y).setZ(zIndex);
    }

    default Widget createWidget() {
        return createWidget(DEF_Z);
    }
}
