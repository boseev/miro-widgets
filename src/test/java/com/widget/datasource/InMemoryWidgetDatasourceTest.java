package com.widget.datasource;

import com.google.common.base.Stopwatch;
import com.widget.WithTestData;
import com.widget.configuration.properties.ApplicationProperties;
import com.widget.data.Widget;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryWidgetDatasourceTest implements WithTestData {
    private static final Logger logger = LoggerFactory.getLogger(InMemoryWidgetDatasourceTest.class);
    private InMemoryWidgetDatasource datasource = new InMemoryWidgetDatasource(new ApplicationProperties());

    @Before
    public void before() {
        datasource.clear();
    }


    @Test
    public void insertTest() {
        Widget newWidget = datasource.save(createWidget());
        assertThat(newWidget.getId()).isNotNull();
        assertThat(newWidget).isEqualToIgnoringGivenFields(createWidget(), "id", "modificationDate");
    }

    @Test(timeout = 30_000)
    public void multithreadingInsertExpectedProperCount() throws Exception {
        int threads = 8;
        int cycles = 100;
        int totalElements = threads*cycles;

        List<CompletableFuture> futures = new ArrayList<>(totalElements);
        IntStream.range(0, threads)
                .forEach(n -> {
                    CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                        IntStream.range(0, cycles).forEach(cycle -> {
                            int coords = n * cycle;
                            datasource.save(new Widget().setX(coords).setY(coords).setZ(coords));
                        });
                    });
                    futures.add(future);
                });

        Stopwatch stopwatch = Stopwatch.createStarted();

        // get limited by test timeout
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[]{})).get();

        //check the storage
        assertThat(datasource.findAll()).hasSize(totalElements);
        logger.debug("Test completed in {} ms", stopwatch.stop().elapsed(TimeUnit.MILLISECONDS));
    }


    @Test(expected = NullPointerException.class)
    public void findByIdPassNullExpectedError() {
        datasource.findById(null);
    }

    @Test
    public void findByIdExpectedValue() {
        Widget save = datasource.save(createWidget());
        assertThat(save.getId()).isNotNull();

        Optional<Widget> byId = datasource.findById(save.getId());
        assertThat(byId).isPresent().get().isEqualToIgnoringGivenFields(save);
    }

    @Test
    public void updateTestExpectedValue() {
        Widget save = datasource.save(createWidget());
        Widget update = new Widget().setId(save.getId()).setX(66).setY(66).setZ(10);
        datasource.save(update);
        Optional<Widget> byId = datasource.findById(save.getId());

        assertThat(byId).isPresent().get().isEqualTo(update);
    }

    @Test(expected = NullPointerException.class)
    public void removeTestPassNullExpectedError() {
        datasource.delete(null);
    }

    @Test
    public void removeTestExpectedRemoval() {
        Widget one = datasource.save(createWidget());
        Widget two = datasource.save(createWidget());
        assertThat(datasource.findAll()).contains(one, two);
        datasource.delete(one.getId());
        assertThat(datasource.findAll()).contains(two);
    }

    @Test
    public void findAllWithOrdering() {
        datasource.save(createWidget(10));
        datasource.save(createWidget(5));
        datasource.save(createWidget(9));
        datasource.save(createWidget(25));
        datasource.save(createWidget(30));
        datasource.save(createWidget(22));

        Collection<Widget> data = datasource.findAll();
        assertThat(data).hasSize(6);
        List<Integer> zIndexList =
                data.stream().map(Widget::getZ).collect(Collectors.toList());

        assertThat(zIndexList).containsExactly(5, 9, 10, 22, 25, 30);
    }
}
