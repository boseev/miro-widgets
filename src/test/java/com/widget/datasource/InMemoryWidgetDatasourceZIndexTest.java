package com.widget.datasource;

import com.widget.WithTestData;
import com.widget.configuration.properties.ApplicationProperties;
import com.widget.data.Widget;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryWidgetDatasourceZIndexTest implements WithTestData {

    private InMemoryWidgetDatasource datasource = new InMemoryWidgetDatasource(new ApplicationProperties());

    @Before
    public void before() {
        datasource.clear();
    }

    @Test
    public void insertNoZIndexExpectedTop() {
        Widget one = datasource.save(createWidget(null));
        assertThat(one.getZ()).isEqualTo(0);

        Widget two = datasource.save(createWidget(-10));
        assertThat(two.getZ()).isEqualTo(-10);

        Widget three = datasource.save(createWidget(null));
        assertThat(three.getZ()).isEqualTo(10);

        Widget four = datasource.save(createWidget(null));
        assertThat(four.getZ()).isEqualTo(20);
    }

    @Test
    public void insertZIndexInTheMiddle() {
        Widget idx5_5 = datasource.save(createWidget(5));
        Widget idx10_20 = datasource.save(createWidget(10));
        Widget idx20_30 = datasource.save(createWidget(20));
        Widget idx11_21 = datasource.save(createWidget(11));
        Widget idx15_25 = datasource.save(createWidget(15));
        Widget idx10_10 = datasource.save(createWidget(10));

        assertThat(datasource.findAll()).hasSize(6)
                .containsExactly(idx5_5, idx10_10, idx10_20, idx11_21, idx15_25, idx20_30);

        assertThat(idx5_5.getZ()).isEqualTo(5);
        assertThat(idx10_10.getZ()).isEqualTo(10);
        assertThat(idx10_20.getZ()).isEqualTo(20);
        assertThat(idx11_21.getZ()).isEqualTo(21);
        assertThat(idx15_25.getZ()).isEqualTo(25);
        assertThat(idx20_30.getZ()).isEqualTo(30);
    }

    @Test
    public void insertZIndexToStart() {
        Widget idx0_20 = datasource.save(createWidget(0));
        Widget idx2_22 = datasource.save(createWidget(2));
        Widget idx4_24 = datasource.save(createWidget(4));
        Widget idx6_26 = datasource.save(createWidget(6));
        Widget idx0_10 = datasource.save(createWidget(0));
        Widget idx0_0 = datasource.save(createWidget(0));

        assertThat(datasource.findAll()).hasSize(6)
                .containsExactly(idx0_0, idx0_10, idx0_20, idx2_22, idx4_24, idx6_26);

        assertThat(idx0_0.getZ()).isEqualTo(0);
        assertThat(idx0_10.getZ()).isEqualTo(10);
        assertThat(idx0_20.getZ()).isEqualTo(20);
        assertThat(idx2_22.getZ()).isEqualTo(22);
        assertThat(idx4_24.getZ()).isEqualTo(24);
        assertThat(idx6_26.getZ()).isEqualTo(26);
    }

    @Test
    public void insertZIndexNegative() {
        Widget idx2_2 = datasource.save(createWidget(2));
        Widget idx4_4 = datasource.save(createWidget(4));
        Widget idx6_6 = datasource.save(createWidget(6));
        Widget idx_neg_2_neg_2 = datasource.save(createWidget(-2));

        assertThat(datasource.findAll()).hasSize(4)
                .containsExactly(idx_neg_2_neg_2, idx2_2, idx4_4, idx6_6);

        assertThat(idx_neg_2_neg_2.getZ()).isEqualTo(-2);
        assertThat(idx2_2.getZ()).isEqualTo(2);
        assertThat(idx4_4.getZ()).isEqualTo(4);
        assertThat(idx6_6.getZ()).isEqualTo(6);

        //add one more with minus
        Widget idx_neg_4_neg_4 = datasource.save(createWidget(-4));

        assertThat(datasource.findAll()).hasSize(5)
                .containsExactly(idx_neg_4_neg_4, idx_neg_2_neg_2, idx2_2, idx4_4, idx6_6);

        // add more -4
        Widget idx_more_neg_4 = datasource.save(createWidget(-4));
        assertThat(datasource.findAll()).hasSize(6)
                .containsExactly(idx_more_neg_4, idx_neg_4_neg_4, idx_neg_2_neg_2, idx2_2, idx4_4, idx6_6);

        // check indexes
        assertThat(idx_more_neg_4.getZ()).isEqualTo(-4);
        assertThat(idx_neg_4_neg_4.getZ()).isEqualTo(6);
        assertThat(idx_neg_2_neg_2.getZ()).isEqualTo(8);
        assertThat(idx2_2.getZ()).isEqualTo(12);
        assertThat(idx4_4.getZ()).isEqualTo(14);
        assertThat(idx6_6.getZ()).isEqualTo(16);

    }

    @Test
    public void insertAndUpdateZIndexExpectedShift() {
        Widget idx2_2 = datasource.save(createWidget(2));
        Widget idx4_4 = datasource.save(createWidget(4));
        Widget idx6_6 = datasource.save(createWidget(6));

        assertThat(datasource.findAll()).hasSize(3)
                .containsExactly(idx2_2, idx4_4, idx6_6);

        // update 4 to 2
        Widget idx4_to_2 = createWidget(2).setId(idx4_4.getId());
        datasource.save(idx4_to_2);

        assertThat(datasource.findAll()).hasSize(3)
                .containsExactly(idx4_to_2, idx2_2, idx6_6);

        assertThat(idx4_to_2.getZ()).isEqualTo(2);
        assertThat(idx2_2.getZ()).isEqualTo(12);
        assertThat(idx6_6.getZ()).isEqualTo(16);
    }

    @Test
    public void insertAndUpdateZIndexNoShift() {
        Widget idx2_2 = datasource.save(createWidget(2));
        Widget idx4_4 = datasource.save(createWidget(4));
        Widget idx6_6 = datasource.save(createWidget(6));

        assertThat(datasource.findAll()).hasSize(3)
                .containsExactly(idx2_2, idx4_4, idx6_6);

        Widget idx4_to_1 = createWidget(1).setId(idx4_4.getId());
        datasource.save(idx4_to_1);

        assertThat(datasource.findAll()).hasSize(3)
                .containsExactly(idx4_to_1, idx2_2, idx6_6);

        assertThat(idx4_to_1.getZ()).isEqualTo(1);
        assertThat(idx2_2.getZ()).isEqualTo(2);
        assertThat(idx6_6.getZ()).isEqualTo(6);
    }

}
