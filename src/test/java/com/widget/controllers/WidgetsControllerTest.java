package com.widget.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.widget.WithTestData;
import com.widget.data.Widget;
import com.widget.datasource.InMemoryWidgetDatasource;
import com.widget.datasource.WidgetDatasource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WidgetsControllerTest implements WithTestData {

    private static final String WIDGETS_BASE_URL = "/api/v1/widgets";
    private static final String GET_ALL_WIDGETS_URL = WIDGETS_BASE_URL + "/all";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private WidgetDatasource datasource;

    @Before
    public void before() {
        if (datasource instanceof InMemoryWidgetDatasource) {
            ((InMemoryWidgetDatasource) datasource).clear();
        }
    }

    @Test
    public void shouldCreateWidget() throws Exception {
        Widget newWidget = createWidget();
        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);

        Widget created = createWidget(newWidget);
        assertThat(created).isEqualToIgnoringGivenFields(newWidget, "id", "modificationDate");
        assertThat(created.getId()).isNotNull();
        assertThat(created.getModificationDate()).isAfter(now);

        // findById
        Widget byId = findById(created.getId());
        assertThat(byId).isEqualTo(created);
    }

    @Test
    public void shouldUpdateWidget() throws Exception {
        Widget created1 = createWidget(createWidget(10));
        Widget created2 = createWidget(createWidget(15));
        // not equals
        assertThat(created1).isNotEqualTo(created2);

        // send update
        Widget updatedWidget = updateWidget(created2.setZ(5));

        // check widgets
        assertThat(updatedWidget).isEqualToIgnoringGivenFields(created2, "z", "modificationDate");
        assertThat(updatedWidget.getModificationDate()).isAfter(created2.getModificationDate());

        // getList of widgets
        List<Widget> allWidgets = getAllWidgets();
        assertThat(allWidgets).hasSize(2).containsExactly(updatedWidget, created1);
    }

    @Test
    public void updateWidgetWrongIdExpectedNoContent() throws Exception {
        pureUpdateWidget(createWidget().setId("dummy_id"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void findByWrongIdExpectedNoContent() throws Exception {
        pureFindById("dummy_id")
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldDeleteWidget() throws Exception {
        Widget created1 = createWidget(createWidget(10));
        Widget created2 = createWidget(createWidget(15));

        List<Widget> allWidgets = getAllWidgets();
        assertThat(allWidgets).hasSize(2).containsExactly(created1, created2);

        // delete
        deleteWidget(created1.getId());

        List<Widget> allWidgetsAfterRemoval = getAllWidgets();
        assertThat(allWidgetsAfterRemoval).hasSize(1).containsExactly(created2);
    }

    private Widget createWidget(Widget newWidget) throws Exception {
        String jsonContent = mvc.perform(
                post(WIDGETS_BASE_URL)
                        .content(mapper.writeValueAsBytes(newWidget))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        return mapper.readValue(jsonContent, Widget.class);
    }


    private Widget findById(String widgetId) throws Exception {
        String jsonContent = pureFindById(widgetId)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        return mapper.readValue(jsonContent, Widget.class);
    }

    private ResultActions pureFindById(String widgetId) throws Exception {
        return mvc.perform(
                get(String.format("%s/%s", WIDGETS_BASE_URL, widgetId))
                        .contentType(MediaType.APPLICATION_JSON));
    }

    private Widget updateWidget(Widget updateWidget) throws Exception {
        String jsonContent = pureUpdateWidget(updateWidget)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        return mapper.readValue(jsonContent, Widget.class);
    }

    private ResultActions pureUpdateWidget(Widget updateWidget) throws Exception {
        return mvc.perform(
                put(WIDGETS_BASE_URL)
                        .content(mapper.writeValueAsBytes(updateWidget))
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    private List<Widget> getAllWidgets() throws Exception {
        String jsonContent = mvc.perform(
                get(GET_ALL_WIDGETS_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);


        return mapper.readValue(jsonContent, new TypeReference<>(){});
    }

    private void deleteWidget(String widgetId) throws Exception {
        mvc.perform(
                delete(String.format("%s/%s", WIDGETS_BASE_URL, widgetId))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
