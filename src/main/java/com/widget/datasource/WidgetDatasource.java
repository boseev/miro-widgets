package com.widget.datasource;

import com.widget.data.Widget;

import java.util.Collection;
import java.util.Optional;

/**
 * Datasource of widgets
 */
public interface WidgetDatasource {

    /**
     * Saves the widget.
     *
     * @param widget must not be {@literal null}.
     *
     * @return the saved entity; will never be {@literal null}.
     * @throws NullPointerException in case the given {@code widget} is {@literal null}
     */
    Widget save(Widget widget);

    /**
     * Retrieves a widget by its id.
     *
     * @param widgetId must not be {@literal null}.
     * @return the saved entities; will never be {@literal null}.
     * @throws NullPointerException in case the given entity is {@literal null}.
     */
    Optional<Widget> findById(String widgetId);

    /**
     * Deletes the widget with the given id.
     *
     * @param widgetId must not be {@literal null}.
     * @throws NullPointerException in case the given {@code id} is {@literal null}
     */
    void delete(String widgetId);

    /**
     * Returns all the widgets.
     *
     * @return all widgets
     */
    Collection<Widget> findAll();
}
