package com.widget.datasource;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

/**
 * Operation synchronization
 */
public class Synchronizer {

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    /**
     * Does read synchronization
     *
     * @param supplier - supplier to call
     * @return supplier output
     */
    public  <T> T doRead(Supplier<T> supplier) {
        Lock readLock = readWriteLock.readLock();
        readLock.lock();
        try {
            return supplier.get();
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Does write synchronization
     *
     * @param supplier - supplier to call
     * @return supplier output
     */
    public <T> T doWrite(Supplier<T> supplier) {
        Lock writeLock = readWriteLock.writeLock();
        writeLock.lock();
        try {
            return supplier.get();
        } finally {
            writeLock.unlock();
        }
    }

}
