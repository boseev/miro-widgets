package com.widget.datasource;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.widget.configuration.properties.ApplicationProperties;
import com.widget.data.Widget;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * In-memory implementation of the widget data source
 */
@Component
public class InMemoryWidgetDatasource implements WidgetDatasource {
    private static final Logger logger = LoggerFactory.getLogger(InMemoryWidgetDatasource.class);

    private final Synchronizer synchronizer = new Synchronizer();
    private final NavigableMap<Integer, String> zIndexStore = new TreeMap<>();
    private final Map<String, Widget> storage = new HashMap<>();
    private final ApplicationProperties applicationProperties;

    @Autowired
    public InMemoryWidgetDatasource(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Widget save(Widget widget) {
        Preconditions.checkNotNull(widget,  "Widget can not be null");
        return synchronizer.doWrite(() -> {
            //empty z-index, set the top one
            if (widget.getZ() == null) {
                widget.setZ(getMaxZIndex());
            }

            if (StringUtils.isBlank(widget.getId())) {
                widget.setId(generateId());
            }

            // check for collision
            String widgetId = zIndexStore.get(widget.getZ());
            if (widgetId != null && !widgetId.equals(widget.getId())) {
                logger.debug("Z-Index collision detected. Old widgetId: '{}', new widget: {}. Shifting...", widgetId, widget);
                processCollisionChange(widget);
            } else {
                logger.debug("Store a new element: {}", widget);
                processRegularChange(widget);
            }

            return widget;
        });
    }

    @Override
    public Optional<Widget> findById(String widgetId) {
        Preconditions.checkNotNull(widgetId,  "Id can not be null");
        return synchronizer.doRead(() -> Optional.ofNullable(storage.get(widgetId)));
    }

    @Override
    public void delete(String widgetId) {
        Preconditions.checkNotNull(widgetId,  "Id can not be null");
        synchronizer.doWrite(() -> {
            Widget remove = storage.remove(widgetId);
            zIndexStore.remove(remove.getZ());
            return null;
        });
    }

    @Override
    public Collection<Widget> findAll() {
        return synchronizer.doRead(() -> zIndexStore.values()
                .stream()
                .map(storage::get)
                .collect(Collectors.toList()));
    }

    /**
     * Does cleanup
     */
    @VisibleForTesting
    public void clear() {
       synchronizer.doWrite(() -> {
           zIndexStore.clear();
           storage.clear();
           return null;
       });
    }

    private String generateId() {
        return UUID.randomUUID().toString();
    }

    /**
     * Get maximal Z-Index
     *
     * @return new Z-Index
     */
    private Integer getMaxZIndex() {
        if (zIndexStore.isEmpty()) {
            return 0;
        } else {
            return zIndexStore.lastKey() + applicationProperties.getzIndexStep();
        }
    }

    /**
     * Does change regarding zIndexCollision
     *
     * @param widget - widget object
     */
    private void processCollisionChange(Widget widget) {
        Optional.ofNullable(storage.get(widget.getId()))
                .ifPresent(w -> {
                    logger.debug("Update with z-index collision detected, old widget: {}, new widget: {}", w, widget);
                    zIndexStore.remove(w.getZ());
                });

        // perform z-Index shift
        LinkedHashMap<Integer, String> tailoredMap = new LinkedHashMap<>(zIndexStore.tailMap(widget.getZ(), true));
        for (Map.Entry<Integer, String> entry : tailoredMap.entrySet()) {
            Integer zIndex = entry.getKey();
            String id = entry.getValue();
            Integer newZIndex = zIndex + applicationProperties.getzIndexStep();

            // remove old and add new
            zIndexStore.remove(zIndex, id);
            zIndexStore.put(newZIndex, id);
            storage.get(id).setZ(newZIndex).setModificationDate(LocalDateTime.now(ZoneOffset.UTC));
        }

        storeElement(widget);
    }

    /**
     * Does change in normal way, no collision
     *
     * @param widget - widget object
     */
    private void processRegularChange(Widget widget) {
        Optional.ofNullable(storage.get(widget.getId()))
                .ifPresent(w -> {
                    logger.debug("Update detected, old widget: {}, new widget: {}", w, widget);
                    zIndexStore.remove(w.getZ());
                });
        storeElement(widget);
    }

    /**
     * Stores element into memory
     *
     * @param widget - widget object
     */
    private void storeElement(Widget widget) {
        widget.setModificationDate(LocalDateTime.now(ZoneOffset.UTC));
        // just do put
        zIndexStore.put(widget.getZ(), widget.getId());
        storage.put(widget.getId(), widget);
    }
}
