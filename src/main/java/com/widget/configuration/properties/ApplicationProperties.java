package com.widget.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Min;

@Component
@ConfigurationProperties(prefix = "widget.service")
public class ApplicationProperties {

    @Min(1)
    private int zIndexStep = 10;

    /**
     * Retrieve the Z-Index step.
     * @return value
     */
    public int getzIndexStep() {
        return zIndexStep;
    }

    public ApplicationProperties setzIndexStep(int zIndexStep) {
        this.zIndexStep = zIndexStep;
        return this;
    }
}
