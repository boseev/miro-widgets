package com.widget.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Index controller, show instead of 404 page
 */
@RestController
public class IndexController {

    @GetMapping
    public String getIndex() {
        return "Backed service is up and running! Try <a href='/swagger'>swagger</a>";
    }

    /**
     * Redirect to swagger-ui
     *
     * @return
     */
    @GetMapping("/swagger")
    public RedirectView swagger() {
        return new RedirectView("/swagger-ui.html");
    }

}
