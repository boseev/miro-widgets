package com.widget.controllers;

import com.google.common.base.Preconditions;
import com.widget.data.Widget;
import com.widget.datasource.WidgetDatasource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Widgets controller
 *
 */
@RestController
@RequestMapping("/api/v1/widgets")
@Api(value = "Widget operations")
public class WidgetsController {

    private static final Logger logger = LoggerFactory.getLogger(WidgetsController.class);

    private final WidgetDatasource widgetDatasource;

    @Autowired
    public WidgetsController(WidgetDatasource widgetDatasource) {
        this.widgetDatasource = widgetDatasource;
    }

    @PostMapping
    @ApiOperation(value = "Create a widget")
    @ResponseBody
    public ResponseEntity<Widget> create(@RequestBody Widget widget) {
        Preconditions.checkNotNull(widget);
        widget.setId(null);
        logger.debug("Creating a widget {}", widget);
        Widget save = widgetDatasource.save(widget);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update a widget")
    public ResponseEntity update(@RequestBody Widget widget) {
        Preconditions.checkNotNull(widget);
        Preconditions.checkNotNull(widget.getId());
        logger.debug("Updating a widget {}", widget);

        return widgetDatasource.findById(widget.getId())
                .map(w -> widgetDatasource.save(widget))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.noContent().build());
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a widget by its id")
    public ResponseEntity delete(@PathVariable("id") String id) {
        Preconditions.checkNotNull(id);
        logger.debug("Removing a widget {}", id);

        widgetDatasource.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a widget by its id")
    public ResponseEntity findById(@PathVariable("id") String id) {
        Preconditions.checkNotNull(id);
        logger.debug("Find widget by id {}", id);

        return widgetDatasource.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.noContent().build());
    }

    @GetMapping("/all")
    @ApiOperation(value = "Get all widgets")
    public Collection<Widget> getAll() {
        logger.debug("Get all widgets");
        return widgetDatasource.findAll();
    }
}
