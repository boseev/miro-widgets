package com.widget.data;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.time.LocalDateTime;

/**
 * Widget entity
 */
public class Widget {
    private String id;
    private int x;
    private int y;
    private Integer z;
    private int width;
    private int height;
    private LocalDateTime modificationDate;

    public String getId() {
        return id;
    }

    public Widget setId(String id) {
        this.id = id;
        return this;
    }

    public int getX() {
        return x;
    }

    public Widget setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Widget setY(int y) {
        this.y = y;
        return this;
    }

    public Integer getZ() {
        return z;
    }

    public Widget setZ(Integer z) {
        this.z = z;
        return this;
    }

    public LocalDateTime getModificationDate() {
        return modificationDate;
    }

    public Widget setModificationDate(LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public Widget setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Widget setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Widget widget = (Widget) o;
        return x == widget.x &&
                y == widget.y &&
                width == widget.width &&
                height == widget.height &&
                Objects.equal(id, widget.id) &&
                Objects.equal(z, widget.z);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, x, y, z, width, height);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("x", x)
                .add("y", y)
                .add("z", z)
                .add("width", width)
                .add("height", height)
                .add("modificationDate", modificationDate)
                .toString();
    }
}
