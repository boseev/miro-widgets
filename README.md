# Widgets backend

##### Author: [Sergey Borovskikh](https://www.linkedin.com/in/sergey-borovskikh-78b49289/)
    
##### Minimal Requirements:

    Java 11
    Maven 3.3.9 

#### About the solution:

Solution is made for working with widget objects, using Java 11, Spring Boot Framework and Maven.

Service integrated with Swagger Framework. Swagger-UI is available via link [http://localhost:8080/swagger](http://localhost:8080/swagger)

![Swagger](swagger-ui.png)

#### Test coverage and build result    

![Test Coverage](build_result.jpg)

